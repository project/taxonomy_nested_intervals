<?php
/**
 * @file
 *
 * Pages for taxonomy nested_intervals settings and more.
 */

/**
 * Form build for the settings form
 *
 * @see taxonomy_nested_intervals_rebuild_submit()
 * @ingroup forms
 */
function taxonomy_nested_intervals_settings_form() {
  $form = array();

  $form['taxonomy_nested_intervals_rebuild_safe_sibling'] = array(
    '#title' => t('Rebuild tree with safe sibling enumeration'),
    '#description' => t('Rebuild tree with safe sibling enumeration'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('taxonomy_nested_intervals_rebuild_safe_sibling', TAXONOMY_NESTED_INTERVALS_REBUILD_SAFE_SIBLING),
  );

  $form = system_settings_form($form);
  return $form;
}

