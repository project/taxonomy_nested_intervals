<?php
/**
 * @file
 *
 * This file contains the functions for reuilding various tables.
 */

/**
 * Start batch job for rebuild of edges
 */
function taxonomy_nested_intervals_rebuild_batch($vids) {
  $batch = array(
    'finished' => 'taxonomy_nested_intervals_rebuild_finished',
    'file' => drupal_get_path('module', 'taxonomy_nested_intervals') . '/taxonomy_nested_intervals.rebuild.inc',
    'title' => t('Rebuilding taxonomy nested intervals'),
    'init_message' => t('Rebuilding taxonomy nested intervals'),
  );
  foreach ($vids as $vid) {
    $batch['operations'][] = array('taxonomy_nested_intervals_rebuild_count_depth', array($vid));
    $batch['operations'][] = array(
      variable_get('taxonomy_nested_intervals_rebuild_safe_sibling', TAXONOMY_NESTED_INTERVALS_REBUILD_SAFE_SIBLING) ? 
      'taxonomy_nested_intervals_rebuild_enumerate_siblings_safe' : 'taxonomy_nested_intervals_rebuild_enumerate_siblings',
      array($vid)
    );
    $batch['operations'][] = array('taxonomy_nested_intervals_rebuild_matrices', array($vid));
  }
  batch_set($batch);
}

/**
 * Finished function for rebuild tree batch operation.
 *
 * @param type $success
 * @param type $result
 * @param type $operations
 */
function taxonomy_nested_intervals_rebuild_finished($success, $results, $operations) {
  if ($success) {
    // Here we do something meaningful with the results.
    $message = theme('item_list', array('items' => $results));
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
  }
  drupal_set_message($message, 'status');
}

/**
 * Count depth of tree.
 */
function taxonomy_nested_intervals_rebuild_count_depth($vid, &$context) {
  if (!isset($context['sandbox']['depth'])) {
    // First time: Clear out rebuild table, and initialize it tid:0
    db_delete('taxonomy_nested_intervals_rebuild')->condition('vid', $vid)->execute();
    $class = variable_get("taxonomy_nested_intervals_method_$vid", TAXONOMY_NESTED_INTERVALS_METHOD);
    $matrix = $class::getDeterminant();
    #db_query("INSERT INTO {taxonomy_nested_intervals_rebuild} (vid, tid, parent, nv, dv, snv, sdv, distance) VALUES($vid, 0, 0, 0, 1, 1, 0, -1)");
    #db_query("INSERT INTO {taxonomy_nested_intervals_rebuild} (vid, tid, parent, nv, dv, snv, sdv, distance) VALUES($vid, 0, 0, 1, 0, 0, 1, -1)");
    db_query("INSERT INTO {taxonomy_nested_intervals_rebuild} (vid, tid, parent, nv, dv, snv, sdv, distance) VALUES($vid, 0, 0, $matrix->nv, $matrix->dv, $matrix->snv, $matrix->sdv, -1)");
    $context['sandbox']['depth'] = -1;
    $context['finished'] = 0.25;
    $context['message'] = t("Initialized rebuild buffer");
    $context['sandbox']['time'] = microtime(TRUE);
    return;
  }
  $depth = &$context['sandbox']['depth'];

  #$order_by = '';
  $order_by = 'ORDER BY d.weight, d.name';

  // Walk through depth and count
  $result = db_query("INSERT INTO {taxonomy_nested_intervals_rebuild} (parent_id, vid, tid, parent, nv, dv, snv, sdv, distance)
    SELECT i.id, d.vid, h.tid, h.parent, 1, 0, 0, 1, i.distance + 1
    FROM {taxonomy_term_hierarchy} h
    JOIN {taxonomy_term_data} d ON d.tid = h.tid
    JOIN {taxonomy_nested_intervals_rebuild} i ON i.tid = h.parent
    WHERE i.distance = :depth
    AND d.vid = :vid
    AND i.vid = :vid
    $order_by
  ", array(':depth' => $depth, ':vid' => $vid));

  if ($result->rowCount() == 0) {
    $context['results'][] = t('Counted depth %depth - took %time seconds', array(
                                '%depth' => $depth, 
                                '%time' => sprintf("%.08f", microtime(TRUE) - $context['sandbox']['time'])
                            ));
    $context['sandbox']['max_depth'] = $depth;
    $context['sandbox']['depth'] = $depth--;
    $context['finished'] = 1;
  }
  else {
    $depth++;
    $context['message'] = t("Counted depth %depth", array('%depth' => $depth));
    // Since we don't know the depth level beforehand, we use a reciprocal value for progress.
    $context['finished'] = 1 - 1 / ($depth + 2);
  }
}

/**
 * Enumerate the siblings for each child per parent in a safe way.
 * (ensure single step sibling even on multiple parents).
 */
function taxonomy_nested_intervals_rebuild_enumerate_siblings_safe($vid, &$context) {
  $context['sandbox']['time'] = microtime(TRUE);
  $parent = db_query("SELECT id FROM {taxonomy_nested_intervals_rebuild} WHERE vid = $vid AND tid = 0")->fetchField();
  update_sibling($vid, $parent);
  $context['results'][] = t('Enumerated siblings - took %time seconds', array(
                              '%time' => sprintf("%.08f", microtime(TRUE) - $context['sandbox']['time'])
                          ));
}

function update_sibling($vid, $parent = 0, $path = '', $depth = 0) {
  global $class, $factor;

  $result = db_query("SELECT * FROM {taxonomy_nested_intervals_rebuild} WHERE parent_id = $parent ORDER BY id");
  $sibling = 1;
  foreach ($result as $term) {
    $term_path = $path ? "$path.$sibling" : $sibling;
    db_query("UPDATE {taxonomy_nested_intervals_rebuild} SET sibling = $sibling WHERE id = $term->id");
    update_sibling($vid, $term->id, $term_path, $depth + 1);
    #$step = (int)(variable_get("taxonomy_nested_intervals_scale_$vid", TAXONOMY_NESTED_INTERVALS_SCALE) / ($depth + 1));
    #$step = $step > 0 ? $step : 1;
    $step = (int)(variable_get("taxonomy_nested_intervals_scale_$vid", TAXONOMY_NESTED_INTERVALS_SCALE));
    $sibling += $step;
  }
}

/**
 * Enumerate the siblings for each child per parent.
 * @todo Fix sibling reset for multiple parents?
 */
function taxonomy_nested_intervals_rebuild_enumerate_siblings($vid, &$context) {
  $context['sandbox']['time'] = microtime(TRUE);

  $scale = variable_get("taxonomy_nested_intervals_scale_$vid", TAXONOMY_NESTED_INTERVALS_SCALE);

  $table = db_query_temporary("SELECT i.id as rebuild_id, (i.id - p.start_id) * $scale + 1 AS sibling
  FROM {taxonomy_nested_intervals_rebuild} i
  JOIN (
    SELECT i.parent, min(i.id) AS start_id
    FROM {taxonomy_nested_intervals_rebuild} i
    WHERE i.vid = $vid AND i.tid > 0
    GROUP BY parent
  ) p ON p.parent = i.parent
  WHERE i.vid = $vid
  AND i.tid > 0");

  // Add index to make update go exponentially faster (very obvious on large trees).
  db_add_primary_key($table, array('rebuild_id'));

  db_query("UPDATE {taxonomy_nested_intervals_rebuild} 
    SET sibling = (select sibling from {" . $table . "} WHERE rebuild_id = id)
    WHERE vid = $vid AND tid > 0");
  $context['results'][] = t('Enumerated siblings - took %time seconds', array(
                              '%time' => sprintf("%.08f", microtime(TRUE) - $context['sandbox']['time'])
                          ));
  $context['finished'] = 1;
}

/**
 * Generate matrices
 */
function taxonomy_nested_intervals_rebuild_matrices($vid, &$context) {
  $factor = variable_get("taxonomy_nested_intervals_factor_$vid", TAXONOMY_NESTED_INTERVALS_FACTOR);
  if (!isset($context['sandbox']['depth'])) {
    // First time: Clear out table, and generate matrices one depth at a time
    db_delete('taxonomy_nested_intervals')->condition('vid', $vid)->execute();
    db_query("INSERT INTO {taxonomy_nested_intervals} (rebuild_id, vid, tid, parent, distance, nv, dv, snv, sdv, buffer_nv, buffer_snv)
    SELECT id, vid, tid, parent, distance, nv, dv, snv, sdv, 1, $factor FROM {taxonomy_nested_intervals_rebuild} WHERE vid = $vid AND tid = 0");
    $context['sandbox']['depth'] = 0;
    $context['sandbox']['max_depth'] = db_query("SELECT MAX(distance) FROM {taxonomy_nested_intervals_rebuild} WHERE vid = $vid")->fetchField() + 2;
    $context['finished'] = ($context['sandbox']['depth'] + 1) / $context['sandbox']['max_depth'];
    $context['message'] = t("Initialized matrix generation");
    $context['sandbox']['time'] = microtime(TRUE);
    return;
  }
  $depth = &$context['sandbox']['depth'];


  // Walk through depth, calculating matrix per child based on childs sibling and parents matrix
  // We cheat a little. We store the a fixed "1" in the buffer_nv to avoid adding a constant to the sibling,
  // as this may type-cast wrong, resulting integer-only arithmetic instead of large number arithemetic as
  // provided by the DECIMAL type.
  // We store the factor in buffer_snv for the same reason.
  $debug1 = $debug2 = '';
  $debug1 = ', path';
  $debug2 = ', CONCAT(i2.path, \'.\', ROUND(i.sibling))';

  $method = variable_get("taxonomy_nested_intervals_method_$vid", TAXONOMY_NESTED_INTERVALS_METHOD);

  // @todo ... smelly code ... well, smellier than the rest, OK!
  if ($method == 'MatrixTreeNodeModified') {
    $sql_nv = '(i2.nv + i2.snv * i.sibling)';
    $sql_dv = '(i2.dv + i2.sdv * i.sibling)';
    $sql_snv = '(i2.nv + i2.snv * (i.sibling + i2.buffer_nv))';
    $sql_sdv = '(i2.dv + i2.sdv * (i.sibling + i2.buffer_nv))';
    $sql_lft = "ROUND($sql_nv * buffer_snv / $sql_dv, 30)";
    $sql_rgt = "ROUND($sql_snv * buffer_snv / $sql_sdv, 30)";
  }

  if ($method == 'MatrixTreeNode') {
    $sql_nv = '(i2.nv * i.sibling + i2.snv)';
    $sql_dv = '(i2.dv * i.sibling + i2.sdv)';
    $sql_snv = '(i2.nv)';
    $sql_sdv = '(i2.dv)';
    $sql_lft = "ROUND($sql_nv * buffer_snv / $sql_dv, 30)";
    $sql_rgt = "ROUND(($sql_nv + $sql_snv) * buffer_snv / ($sql_dv + $sql_sdv), 30)";

    if ($depth % 2 != 0) {
      list($sql_lft, $sql_rgt) = array($sql_rgt, $sql_lft);
    }
    else {
      $sql_lft .= ' + (CASE i.sibling WHEN 1 THEN 0.000000000000000000000000000001 ELSE 0 END)';
    }
  }

  $result = db_query("INSERT INTO {taxonomy_nested_intervals} (rebuild_id, vid, tid, parent, distance, buffer_nv, buffer_snv, nv, dv, snv, sdv, lft, rgt$debug1)
    SELECT i.id, i.vid, i.tid, i.parent, i.distance, 1, $factor
    , $sql_nv AS nv
    , $sql_dv AS dv
    , $sql_snv AS snv
    , $sql_sdv AS sdv
    , $sql_lft AS lft
    , $sql_rgt AS rgt
    $debug2
    FROM {taxonomy_nested_intervals_rebuild} i
    JOIN {taxonomy_term_data} d ON d.tid = i.tid
    JOIN {taxonomy_nested_intervals} i2 ON i2.rebuild_id = i.parent_id
    WHERE i.vid = $vid
    AND i.distance = $depth");

  $context['sandbox']['terms'] += $result->rowCount();

  $context['message'] = t("Generated matrices for %terms terms", array('%terms' => $context['sandbox']['terms']));;

  $depth++;
  $context['finished'] = ($depth + 1) / $context['sandbox']['max_depth'];

  if ($context['finished'] >= 1) {
    $context['results'][] = t('Generated matrices for %terms terms - took %time seconds', array(
                                '%terms' => $context['sandbox']['terms'],
                                '%time' => sprintf("%.08f", microtime(TRUE) - $context['sandbox']['time'])
                            ));
    $context['finished'] = 1;
    return;
  }
}

/**
 * Update left/right values.
 */
function taxonomy_nested_intervals_rebuild_left_right($vid, &$context) {
  $context['sandbox']['time'] = microtime(TRUE);

  $factor = variable_get("taxonomy_nested_intervals_factor_$vid", TAXONOMY_NESTED_INTERVALS_FACTOR);
  $class = variable_get("taxonomy_nested_intervals_method_$vid", TAXONOMY_NESTED_INTERVALS_METHOD);

  $sql_lft = "ROUND(nv * $factor / dv, 30)";
  $sql_rgt = "ROUND(snv * $factor / sdv, 30)";

  if ($class == 'MatrixTreeNode') {
    $sql_lft_org = $sql_lft = "ROUND(nv * $factor / dv, 30)";
    $sql_rgt = "ROUND((nv + snv) * $factor / (dv + sdv), 30)";
    $sql_lft = "(CASE distance % 2 WHEN 0 THEN $sql_lft + (CASE IF(snv = 1, dv, FLOOR(nv / snv)) WHEN 1 THEN 0.000000000000000000000000000001 ELSE 0 END) ELSE $sql_rgt END)";
    $sql_rgt = "(CASE distance % 2 WHEN 1 THEN $sql_lft_org ELSE $sql_rgt END)";
  }

  db_query("UPDATE {taxonomy_nested_intervals}
    SET lft = $sql_lft,
        rgt = $sql_rgt
    WHERE vid = $vid
    AND tid > 0");

  $context['results'][] = t('Generated left/right values - took %time seconds', array(
                              '%time' => sprintf("%.08f", microtime(TRUE) - $context['sandbox']['time'])
                          ));
  $context['finished'] = 1;
}
