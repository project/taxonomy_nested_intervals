<?php
/**
 * @file
 *
 * @todo Handle race condition on insert when determining insertion point
 */

// Put on your digits and scale the night away ...
define('TAXONOMY_NESTED_INTERVALS_FACTOR', '10000000000000000000');

// Scale (add gaps for less frequent sibling push and tree rebuilding)
define('TAXONOMY_NESTED_INTERVALS_SCALE', 1);

// Method for generating fractions (Tropashko vs Hazel, defaults to Hazel)
define('TAXONOMY_NESTED_INTERVALS_METHOD', 'MatrixTreeNodeModified');

// Whether or not to ensure correct sibling enumeration when rebuilding tree.
define('TAXONOMY_NESTED_INTERVALS_REBUILD_SAFE_SIBLING', FALSE);


require_once('taxonomy_nested_intervals.math.inc');
require_once('MatrixTreeNode.class.php');

// ---------- HOOKS ----------
/**
 * Implements hook_help().
 */
function taxonomy_nested_intervals_help($section) {
  switch ($section) {
    case 'admin/help#taxonomy_nested_intervals':
      // Return a line-break version of the module README.txt
      return check_markup(file_get_contents( dirname(__FILE__) . "/README.txt"));
  }
}

/**
 * Implements hook_perm().
 */
function taxonomy_nested_intervals_permission() {
  return array(
    'administer taxonomy nested intervals' => array(
      'title' => t('Administer Taxonomy Nested Intervals'),
      'description' => t('Perform administration tasks for Taxonomy Nested Intervals.'),
    )
  );
}
/**
 * Implements hook_menu().
 */
function taxonomy_nested_intervals_menu() {
  $items = array();

  // Bring /level and /all back into taxonomy pages and feeds
  $items['taxonomy/term/%taxonomy_term'] = array(
    'title' => 'Taxonomy term',
    'title callback' => 'taxonomy_term_title',
    'title arguments' => array(2),
    'page callback' => 'taxonomy_nested_intervals_term_page',
    'page arguments' => array(2, 3),
    'access arguments' => array('access content'),
    'file' => 'taxonomy_nested_intervals.pages.inc',
  );
  $items['taxonomy/term/%taxonomy_term/%/feed'] = array(
    'title' => 'Taxonomy term',
    'title callback' => 'taxonomy_term_title',
    'title arguments' => array(2),
    'page callback' => 'taxonomy_nested_intervals_term_feed',
    'page arguments' => array(2, 3),
    'access arguments' => array('access content'),
    'file' => 'taxonomy_nested_intervals.pages.inc',
  );

  // settings page
  $items['admin/structure/taxonomy/nested_intervals'] = array(
    'title'            => 'Nested Intervals',
    'description'      => 'Administer taxonomy nested intervals',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('taxonomy_nested_intervals_settings_form'),
    'access arguments' => array('administer taxonomy nested intervals'),
    'type'             => MENU_LOCAL_TASK,
    'file'             => 'taxonomy_nested_intervals.admin.inc'
  );

  return $items;
}

/**
 * Implements hook_form_N_alter().
 */
function taxonomy_nested_intervals_form_taxonomy_form_vocabulary_alter(&$form, &$form_state) {
  $vid = empty($form_state['vocabulary']->vid) ? '' : '_' . $form_state['vocabulary']->vid;
  $enabled = variable_get("taxonomy_nested_intervals_enabled$vid", FALSE);
  $method = variable_get("taxonomy_nested_intervals_method$vid", TAXONOMY_NESTED_INTERVALS_METHOD);
  $scale = variable_get("taxonomy_nested_intervals_scale$vid", TAXONOMY_NESTED_INTERVALS_SCALE);
  $factor = variable_get("taxonomy_nested_intervals_factor$vid", TAXONOMY_NESTED_INTERVALS_FACTOR);
  $form['taxonomy_nested_intervals'] = array(
    '#title' => t('Nested Intervals'),
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['taxonomy_nested_intervals']['enabled'] = array(
    '#title' => t('Enabled'),
    '#description' => t('Nested Intervals enabled for this vocabulary.'), 
    '#type' => 'checkbox',
    '#default_value' => $enabled,
  );
  $form['taxonomy_nested_intervals']['method'] = array(
    '#title' => t('Encoding'),
    '#description' => t('Method of encoding'), 
    '#type' => 'select',
    '#default_value' => $method,
    '#options' => array(
      'MatrixTreeNode' => t('Original (unsorted space-efficient)'),
      'MatrixTreeNodeModified' => t('Modified (sorted not space efficient)'),
    )
  );
  $form['taxonomy_nested_intervals']['scale'] = array(
    '#title' => t('Scale'),
    '#type' => 'textfield',
    '#description' => t('Scale factor for siblings. Creates gaps between siblings which can help speed up tree update operations.'), 
    '#default_value' => $scale,
  );
  $form['taxonomy_nested_intervals']['factor'] = array(
    '#title' => t('Factor'),
    '#description' => t('Scale factor for left/right values. Use low value for small trees with large depth and large values for trees large trees with small depth.'), 
    '#type' => 'textfield',
    '#default_value' => $factor,
  );
  $form['taxonomy_nested_intervals']['rebuild'] = array(
    '#type' => 'submit',
    '#value' => t('Rebuild'),
    '#submit' => array('taxonomy_nested_intervals_form_taxonomy_form_vocabulary_alter_submit_rebuild'),
  );
  $form['#submit'][] = 'taxonomy_nested_intervals_form_taxonomy_form_vocabulary_alter_submit';
}

/**
 * Submit handler: Save vocabulary based nested intervals settings.
 */
function taxonomy_nested_intervals_form_taxonomy_form_vocabulary_alter_submit($form, &$form_state) {
  if (isset($form_state['vocabulary']->vid) && isset($form_state['values']['taxonomy_nested_intervals'])) {
    $vid = $form_state['vocabulary']->vid;
    $current_state = variable_get("taxonomy_nested_intervals_enabled_$vid", FALSE);
    $current_method = variable_get("taxonomy_nested_intervals_method_$vid", TAXONOMY_NESTED_INTERVALS_METHOD);
    $current_factor = variable_get("taxonomy_nested_intervals_factor_$vid", TAXONOMY_NESTED_INTERVALS_FACTOR);
    variable_set("taxonomy_nested_intervals_enabled_$vid", $form_state['values']['taxonomy_nested_intervals']['enabled']);
    variable_set("taxonomy_nested_intervals_method_$vid", $form_state['values']['taxonomy_nested_intervals']['method']);
    variable_set("taxonomy_nested_intervals_scale_$vid", $form_state['values']['taxonomy_nested_intervals']['scale']);
    variable_set("taxonomy_nested_intervals_factor_$vid", $form_state['values']['taxonomy_nested_intervals']['factor']);
    if (
      !empty($form_state['values']['force_rebuild']) || 
      ($form_state['values']['taxonomy_nested_intervals']['enabled'] && !$current_state) ||
      ($form_state['values']['taxonomy_nested_intervals']['method'] !== $current_method)
    ) {
      module_load_include('rebuild.inc', 'taxonomy_nested_intervals');
      taxonomy_nested_intervals_rebuild_batch(array($vid));
    }
    elseif ($form_state['values']['taxonomy_nested_intervals']['factor'] !== $current_factor) {
      module_load_include('rebuild.inc', 'taxonomy_nested_intervals');
      // Rebuild left/right values
      $batch = array(
        'finished' => 'taxonomy_nested_intervals_rebuild_finished',
        'file' => drupal_get_path('module', 'taxonomy_nested_intervals') . '/taxonomy_nested_intervals.rebuild.inc',
        'title' => t('Rebuilding taxonomy nested intervals'),
        'init_message' => t('Rebuilding taxonomy nested intervals'),
      );
      $batch['operations'][] = array('taxonomy_nested_intervals_rebuild_left_right', array($vid));
      batch_set($batch);
    }
  }
}

/**
 * Submit handler: Start batch job for rebuild of nested intervals.
 */
function taxonomy_nested_intervals_form_taxonomy_form_vocabulary_alter_submit_rebuild($form, &$form_state) {
  $form_state['values']['force_rebuild'] = TRUE;
  taxonomy_nested_intervals_form_taxonomy_form_vocabulary_alter_submit($form, $form_state);
}

/**
 * Implements hook_taxonomy_term_insert().
 */
function taxonomy_nested_intervals_taxonomy_term_insert($term) {
  if (!variable_get("taxonomy_nested_intervals_enabled_$term->vid", FALSE)) {
    return;
  }
  $parents = _taxonomy_nested_intervals_unify_parents($term->parent);
  if ($parents) {
    $tx = db_transaction();
    foreach ($parents as $parent) {
      // Find insertion points.
      $points = _taxonomy_nested_intervals_find_insertion_points($term, $parent);

      if (empty($points)) {
        // If no points located, tree must be invalid ... rebuild?
        watchdog('taxonomy_nested_intervals', 'Tree for vocabulary: @vid is invalid', array('@vid' => $term->vid), WATCHDOG_ERROR);
        return;
      }

      // Insert the new points
      foreach ($points as $point) {
        // Make sure $point fits into the tree
        _taxonomy_nested_intervals_make_space($term->vid, $point['matrix']);

        // Insert the new path
        _taxonomy_nested_intervals_insert_path($point['matrix'], $term);
      }
    }
    unset($tx);
  }
}

/**
 * Implements hook_taxonomy_term_update().
 */
function taxonomy_nested_intervals_taxonomy_term_update($term) {
  if (!variable_get("taxonomy_nested_intervals_enabled_$term->vid", FALSE)) {
    return;
  }
  $class = variable_get("taxonomy_nested_intervals_method_$term->vid", TAXONOMY_NESTED_INTERVALS_METHOD);
  $factor = variable_get("taxonomy_nested_intervals_factor_$term->vid", TAXONOMY_NESTED_INTERVALS_FACTOR);
  $tx = db_transaction();

  if (!isset($term->parent)) {
    // Parent not set, no need to update hierarchy.
    return;
  }

  // Derive proper parents.
  $parents = _taxonomy_nested_intervals_unify_parents($term->parent);

  // Get tree node info for the term (gives us its old parents and points)
  $old_points = db_query("SELECT id, lft, rgt, nv, dv, snv, sdv, parent FROM {taxonomy_nested_intervals} WHERE tid = :tid ORDER BY lft", array(':tid' => $term->tid))->fetchAllAssoc('lft', PDO::FETCH_ASSOC);

  // Locate new insertion points in tree
  $new_points = array();
  foreach ($parents as $parent) {
    // Find insertion points
    $new_points = array_merge($new_points, _taxonomy_nested_intervals_find_insertion_points($term, $parent));
  }


  // Intersect old and new points to determine which points should respectively deleted, moved or added.
  $del_points = $old_points;
  foreach ($del_points as &$point) {
    $point['matrix'] = $class::createFromAssoc($point);
  }

  $add_points = array();
  foreach ($new_points as &$point) {
    $lft = $point['lft'] = $point['matrix']->getLeft($factor);
    if (isset($del_points[$lft])) {
      unset($del_points[$lft]);
    }
    else {
      $add_points[$lft] = $point;
    }
  }
  $mov_points = array();
  reset($del_points);
  reset($add_points);
  while ($del_points && $add_points) {
    $mov_points[] = array(array_shift($del_points), array_shift($add_points));
  }

  $dpm = $mpm = $apm = array();
  foreach ($del_points as $point) { $dpm[] = $point['matrix']->getPath(); }
  foreach ($mov_points as $point) { $mpm[] = $point[0]['matrix']->getPath() . ' => ' . $point[1]['matrix']->getPath(); }
  foreach ($add_points as $point) { $apm[] = $point['matrix']->getPath(); }

  // First we delete the ones that needs to be deleted. That will clear some space in tree, and if we're
  // lucky we don't need to push any siblings because of the gaps it will make.
  foreach ($del_points as $point) {
    $lft = $point['lft'];
    $rgt = $point['rgt'];
    db_query("DELETE FROM {taxonomy_nested_intervals} WHERE vid = :vid AND lft >= $lft AND lft < $rgt", array(
      ':vid' => $term->vid,
    ));
  }

  // Then we move the ones we can move (moving instead of delete/add saves us of some index re-calculation in the db)
  foreach ($mov_points as $mov_point) {
    $src = $mov_point[0];
    $dst = $mov_point[1];

    _taxonomy_nested_intervals_make_space($term->vid, $dst['matrix']);

    // Let's derive the old depth and sibling number
    $src_depth = $src['matrix']->getDepth();
    $src_sibling = $src['matrix']->getEnumeratedSibling();

    // And the new depth and sibling number
    $depth = $dst['matrix']->getDepth();
    $sibling = $dst['matrix']->getEnumeratedSibling();

    // And then calculate the difference which we should use in our matrix transformation
    $sibling -= $src_sibling;

    // And then calculate the difference in distance
    $depth -= $src_depth;

    // $matrices contain the new parent
    $matrix = $src['matrix']->relocate($dst['matrix'], $sibling);

    // Do the locomotion ...
    _taxonomy_nested_intervals_transform_paths($matrix, $depth, $term->vid, $src['lft'], $src['rgt']);
    db_query("UPDATE {taxonomy_nested_intervals} SET parent = :parent WHERE id  = :id", array(
      ':parent' => $dst['parent'],
      ':id' => $src['id']
    ));
  }

  // Then we add the new ones.
  // First we pick an existing subtree to use as a template for copying.
  $src = db_query_range("SELECT id, lft, rgt, nv, dv, snv, sdv, parent FROM {taxonomy_nested_intervals} WHERE tid = :tid", 0, 1, array(':tid' => $term->tid))->fetchAssoc();
  $src['matrix'] = $class::createFromAssoc($src);

  // Let's erive the old depth and sibling number
  $src_depth = $src['matrix']->getDepth();
  $src_sibling = $src['matrix']->getEnumeratedSibling();

  // Copy the template to the relevant destinations
  foreach ($add_points as $dst) {
    _taxonomy_nested_intervals_make_space($term->vid, $dst['matrix']);
    
    // Derive the new depth and sibling number
    $depth = $dst['matrix']->getDepth();
    $sibling = $dst['matrix']->getEnumeratedSibling();

    // And then calculate the difference which we should use in our matrix transformation
    $sibling -= $src_sibling;

    // And then calculate the difference in distance
    $depth -= $src_depth;

    // $matrices contain the new parent
    $matrix = $src['matrix']->relocate($dst['matrix'], $sibling);

    // Do the locomotion ...
    _taxonomy_nested_intervals_copy_paths($matrix, $depth, $term->vid, $src['lft'], $src['rgt']);
    db_query("UPDATE {taxonomy_nested_intervals} SET parent = :parent WHERE tid  = :tid AND parent = :old_parent", array(
      ':tid' => $term->tid,
      ':parent' => $dst['parent'],
      ':old_parent' => $src['parent']
    ));
  }
}

/**
 * Implements hook_taxonomy_term_delete().
 */
function taxonomy_nested_intervals_taxonomy_term_delete($term) {
  if (!variable_get("taxonomy_nested_intervals_enabled_$term->vid", FALSE)) {
    return;
  }
  db_query("DELETE FROM {taxonomy_nested_intervals} WHERE tid = :tid", array(':tid' => $term->tid));
}

/**
 * Implements hook_form_N_alter().
 *
 * Fix missing parent=0 on multiparent edit
 */
function taxonomy_nested_intervals_form_taxonomy_form_term_alter(&$form, &$form_state) {
  if (!isset($form['relations']['parent']['#default_value'])) {
    return;
  }
  $term = $form['#term'];
  if ($term && $form['relations']['parent']['#default_value'] != array(0) && is_numeric($term['tid'])) {
    if (db_query("SELECT 1 FROM {taxonomy_term_hierarchy} WHERE tid = :tid AND parent = 0", array(':tid' => $term['tid']))->fetchField()) {
      array_unshift($form['relations']['parent']['#default_value'], 0);
    }
  }
}

// ---------- PRIVATE HELPER FUNCTIONS ----------

/**
 * Find insertion point(s) for a term.
 *
 * If we have the tree:
 * term1
 * term1 > term2
 * term1 > term3
 * term4
 * term4 > term1
 * term4 > term1 > term2
 * term4 > term1 > term3
 *
 * and we want to insert "term21" with the parent "term1", then this function will return
 * the nested interval points for:
 *
 * term1
 * term1 > term2
 * term1 > term21 (new point)
 * term1 > term3
 * term4
 * term4 > term1
 * term4 > term1 > term2
 * term4 > term1 > term21 (new point)
 * term4 > term1 > term3
 *
 * @param $term
 *   Term to find insertion point(s) for. Must contain ->tid, ->name and ->weight
 * @param $parent
 *   Parent term id for $term 
 *
 * @param $points
 *   Matrices of the new points for $term
 */
function _taxonomy_nested_intervals_find_insertion_points($term, $parent) {
  $points = array();
  $class = variable_get("taxonomy_nested_intervals_method_$term->vid", TAXONOMY_NESTED_INTERVALS_METHOD);
  $factor = variable_get("taxonomy_nested_intervals_factor_$term->vid", TAXONOMY_NESTED_INTERVALS_FACTOR);
  $scale = variable_get("taxonomy_nested_intervals_factor_$term->vid", TAXONOMY_NESTED_INTERVALS_SCALE);

  // Find insertion points at specific level in tree (i.e. previous sibling for this parent)
  #if (variable_get("taxonomy_nested_intervals_sort_$term->vid", TRUE)) {
  if (TRUE) {
    $query = db_select('taxonomy_term_data', 'd')
      ->fields('d', array('tid'))
      ->condition('d.vid', $term->vid)
      ->condition('d.tid', $term->tid, '<>')
      ->orderBy('d.weight', 'DESC')
      ->orderBy('d.name', 'DESC')
      ->range(0, 1);
      $query->join('taxonomy_term_hierarchy', 'h', 'h.tid = d.tid AND h.parent = :parent', array(':parent' => $parent));
      $query->where('(d.weight < :weight1 OR (d.weight = :weight2 AND d.name < :name))', array(
        ':weight1' => $term->weight,
        ':weight2' => $term->weight,
        ':name' => $term->name,
      ));
    $tid = $query->execute()->fetchField();
    if ($tid) {
      // Previous sibling found!
      $points = db_query("SELECT nv, dv, snv, sdv FROM {taxonomy_nested_intervals} WHERE tid = :tid AND parent = :parent", array(
        ':tid' => $tid,
        ':parent' => $parent,
      ))->fetchAll(PDO::FETCH_ASSOC);
    }
  }
  else {
    if ($parent == 0) {
      $points = db_query_range("SELECT nv, dv, snv, sdv FROM {taxonomy_nested_intervals} WHERE vid = :vid AND distance = 0 ORDER BY lft DESC", 0, 1, array(
        ':vid' => $term->vid
      ))->fetchAll(PDO::FETCH_ASSOC);
    }
    else {
      $parents = db_query("SELECT lft, rgt, distance FROM {taxonomy_nested_intervals} WHERE tid = :tid", array(':tid' => $parent))->fetchAll(PDO::FETCH_ASSOC);
      foreach ($parents as $point) {
        $lft = $point['lft'];
        $rgt = $point['rgt'];
        $distance = $point['distance'] + 1;
        $points[] = db_query_range("SELECT nv, dv, snv, sdv FROM {taxonomy_nested_intervals} WHERE vid = $term->vid AND lft > $lft AND lft < $rgt AND distance = $distance ORDER BY lft DESC", 0 ,1)->fetchAssoc();
      }
    }
  }

  if ($points) {
    foreach ($points as &$point) {
      $matrix = $class::createFromAssoc($point);
      $depth = $matrix->getDepth();
      $point['matrix'] = $matrix;

      // Calculate parent (needed for sibling bump)
      $parent_matrix = $matrix->getSafeParent();

      $sibling = 1;
      // If no next sibling, then scale if we're not the first (and only) and this is a sorted tree
      if ($class == 'MatrixTreeNodeModified' && $matrix->getEnumeratedSibling() > 1) {
        $query = db_select('taxonomy_nested_intervals', 'i')
          ->fields('i', array('id'))
          ->condition('i.vid', $term->vid)
          ->condition('i.lft', $point['matrix']->getLeft($factor), '>')
          ->condition('i.distance', $depth);
        if ($depth > 0) { 
          $query->condition('i.rgt', $parent_matrix->getRight($factor), '<');
        }
        if (!$query->execute()->fetchField()) {
          $sibling += $scale - 1;
        }
      }

      // Bump sibling
      $point['matrix'] = $matrix->generateSibling($matrix->getEnumeratedSibling() + $sibling);
      $point['parent'] = $parent;
    }
  }
  elseif ($parent == 0) {
    // No parent and no previous siblings ... so this must be the very top most node
    $point['matrix'] = $class::pathToMatrix('1');
    $point['parent'] = $parent;
    $points = array($point);
  }
  else {
    // No previous siblings found, meaning that this term the first sibling in this level
    $points = db_query("SELECT nv, dv, snv, sdv FROM {taxonomy_nested_intervals} WHERE tid = :tid", array(':tid' => $parent))->fetchAll(PDO::FETCH_ASSOC);

    foreach ($points as &$point) {
      $matrix = $class::createFromAssoc($point);
      // Calculate current sibling (i.e. parents first sibling)
      $point['matrix'] = $matrix->generateLeaf(1);
      $point['parent'] = $parent;
    }
  }
  return $points;
}


/**
 * Apply a matrix multiplication for all paths (matrices) in scope vid:[lft;rgt[
 *
 * @param $matrix
 *   Matrix to apply
 * @param $vid
 *   Vocabulary ID of scope
 * @param $lft
 *   Left of scope
 * @param $rgt
 *   Right of scope (0 = inf: [lft;inf[)
 */
function _taxonomy_nested_intervals_transform_paths($matrix, $delta_depth, $vid, $lft, $rgt = 0) {
  $factor = variable_get("taxonomy_nested_intervals_factor_$vid", TAXONOMY_NESTED_INTERVALS_FACTOR);
  $class = variable_get("taxonomy_nested_intervals_method_$vid", TAXONOMY_NESTED_INTERVALS_METHOD);

  $sql_nv = "($matrix->nv * buffer_nv + $matrix->snv * dv)";
  $sql_dv = "($matrix->dv * buffer_nv + $matrix->sdv * dv)";
  $sql_snv = "($matrix->nv * buffer_snv + $matrix->snv * sdv)";
  $sql_sdv = "($matrix->dv * buffer_snv + $matrix->sdv * sdv)";

  $sql_lft = "ROUND(nv * $factor / dv, 30)";
  $sql_rgt = "ROUND(snv * $factor / sdv, 30)";

  if ($class == 'MatrixTreeNode') {
    if (BigNum::cmp($matrix->nv, 0) === -1) {
      $sql_nv = "-$sql_nv";
      $sql_dv = "-$sql_dv";
      $sql_snv = "-$sql_snv";
      $sql_sdv = "-$sql_sdv";
    }
    $sql_lft_org = $sql_lft = "ROUND(nv * $factor / dv, 30)";
    $sql_rgt = "ROUND((nv + snv) * $factor / (dv + sdv), 30)";
    $sql_lft = "(CASE distance % 2 WHEN 0 THEN $sql_lft + (CASE IF(snv = 1 AND sdv <> 0, dv, FLOOR(nv / snv)) WHEN 1 THEN 0.000000000000000000000000000001 ELSE 0 END) ELSE $sql_rgt END)";
    $sql_rgt = "(CASE distance % 2 WHEN 1 THEN $sql_lft_org ELSE $sql_rgt END)";
  }

  $rgt = $rgt ? " AND lft < $rgt" : '';

  $sql = "UPDATE {taxonomy_nested_intervals}
     SET buffer_nv = nv,
         buffer_snv = snv,
         nv  = $sql_nv,
         dv  = $sql_dv,
         snv = $sql_snv,
         sdv = $sql_sdv,
         distance = distance + $delta_depth,
         lft = $sql_lft,
         rgt = $sql_rgt
      WHERE vid = $vid 
      AND lft >= $lft
      $rgt
  ";

  db_query($sql);
}

/**
 * Copy all paths (matrices) in scope vid:[lft;rgt[ while applying a matrix multiplication
 *
 * @param $matrix
 *   Matrix to apply
 * @param $vid
 *   Vocabulary ID of scope
 * @param $lft
 *   Left of scope
 * @param $rgt
 *   Right of scope (0 = inf: [lft;inf[)
 */
function _taxonomy_nested_intervals_copy_paths($matrix, $delta_depth, $vid, $lft, $rgt = 0) {
  $factor = variable_get("taxonomy_nested_intervals_factor_$vid", TAXONOMY_NESTED_INTERVALS_FACTOR);
  $class = variable_get("taxonomy_nested_intervals_method_$vid", TAXONOMY_NESTED_INTERVALS_METHOD);

  $sql_nv = "($matrix->nv * nv + $matrix->snv * dv)";
  $sql_dv = "($matrix->dv * nv + $matrix->sdv * dv)";
  $sql_snv = "($matrix->nv * snv + $matrix->snv * sdv)";
  $sql_sdv = "($matrix->dv * snv + $matrix->sdv * sdv)";

  $sql_lft = "ROUND($sql_nv * $factor / $sql_dv, 30)";
  $sql_rgt = "ROUND($sql_snv * $factor / $sql_sdv, 30)";

  if ($class == 'MatrixTreeNode') {
    if (BigNum::cmp($matrix->nv, 0) === -1) {
      $sql_nv = "-$sql_nv";
      $sql_dv = "-$sql_dv";
      $sql_snv = "-$sql_snv";
      $sql_sdv = "-$sql_sdv";
    }
    $sql_lft_org = $sql_lft = "ROUND($sql_nv * $factor / $sql_dv, 30)";
    $sql_rgt = "ROUND(($sql_nv + $sql_snv) * $factor / ($sql_dv + $sql_sdv), 30)";
    $sql_lft = "(CASE (distance + $delta_depth) % 2 WHEN 0 THEN $sql_lft + (CASE IF(snv = 1 AND sdv <> 0, dv, FLOOR(nv / snv)) WHEN 1 THEN 0.000000000000000000000000000001 ELSE 0 END) ELSE $sql_rgt END)";
    $sql_rgt = "(CASE (distance + $delta_depth) % 2 WHEN 1 THEN $sql_lft_org ELSE $sql_rgt END)";
  }

  $rgt = $rgt ? " AND lft < $rgt" : '';

  $sql = "INSERT INTO {taxonomy_nested_intervals} (vid, tid, parent, nv, dv, snv, sdv, distance, lft, rgt)
  SELECT s.vid, s.tid, s.parent, s.nv, s.dv, s.snv, s.sdv, s.distance, s.lft, s.rgt
  FROM (
    SELECT vid, tid, parent,
           $sql_nv AS nv,
           $sql_dv AS dv,
           $sql_snv AS snv,
           $sql_sdv AS sdv,
           $sql_lft AS lft,
           $sql_rgt AS rgt,
           distance + $delta_depth AS distance
           FROM {taxonomy_nested_intervals}
           WHERE vid = $vid
           AND lft >= $lft
           $rgt
    ) s";

  db_query($sql);
}

/**
 * Insert term at a specific point.
 *
 * @param $matrix
 *   Point in tree.
 * @param $term
 *   Term to insert at the point.
 */
function _taxonomy_nested_intervals_insert_path($matrix, $term) {
  $factor = variable_get("taxonomy_nested_intervals_factor_$term->vid", TAXONOMY_NESTED_INTERVALS_METHOD);
  // Insert new term
  db_query("INSERT INTO {taxonomy_nested_intervals} (vid, tid, parent, nv, dv, snv, sdv, distance, lft, rgt)
  VALUES(:vid, :tid, :parent, :nv, :dv, :snv, :sdv, :distance, :lft, :rgt)", array(
    ':vid' => $term->vid,
    ':tid' => $term->tid,
    ':parent' => $term->parent,
    ':nv' => $matrix->nv, 
    ':dv' => $matrix->dv,
    ':snv' => $matrix->snv,
    ':sdv' => $matrix->sdv,
    ':distance' => $matrix->getDepth(),
    ':lft' => $matrix->getLeft($factor),
    ':rgt' => $matrix->getRight($factor)
  ));
}

/**
 * Push siblings if necessary.
 *
 * If a point already exists at $point, then push that and all it's siblings (plus children),
 ' to make room for the new $point.
 *
 * @param $vid
 *   Vocabulary ID
 * @param $point
 *   Matrix of point to make room for
 */
function _taxonomy_nested_intervals_make_space($vid, $matrix) {
  $class = variable_get("taxonomy_nested_intervals_method_$vid", TAXONOMY_NESTED_INTERVALS_METHOD);
  $factor = variable_get("taxonomy_nested_intervals_factor_$vid", TAXONOMY_NESTED_INTERVALS_FACTOR);
  $scale = variable_get("taxonomy_nested_intervals_scale_$vid", TAXONOMY_NESTED_INTERVALS_SCALE);

  // Derive parent, if not specified
  $parent = $matrix->getSafeParent();

  // Calculate scope of sibling bump = [our left;parent right[
  $lft = $matrix->getLeft($factor);

  // Only bump if there is no gap
  if (db_query("SELECT id FROM {taxonomy_nested_intervals} WHERE vid = :vid AND lft = $lft", array(':vid' => $vid))->fetchField()) {
    // Calculate scope of sibling bump = [our left;parent right[
    $rgt = $matrix->getDepth() > 0 ? $parent->getRight($factor) : 0;

    // Matrix for bumping siblings one step (parent * step * inverse parent)
    $bump = $matrix->relocate($matrix, $scale);

    // Transform that scope (bump siblings)
    _taxonomy_nested_intervals_transform_paths($bump, 0, $vid, $lft, $rgt);
  }
}

/**
 * Unify parents
 *
 * @param mixed $parents
 * @return array
 *   Flattened array of parent term IDs
 */
function _taxonomy_nested_intervals_unify_parents($parents) {
  $parents = is_array($parents) ? $parents : array($parents);
  $new_parents = array();
  foreach ($parents as $parent) {
    if (is_array($parent)) {
      foreach ($parent as $new) {
        $new_parents[] = $new;
      }
    }
    else {
      $new_parents[] = $parent;
    }
  }
  return $new_parents;
}


// ---------- PUBLIC HELPER FUNCTIONS ----------

/**
 * Get max depth of a tree..
 *
 * @param $vid
 *   Vocabulary ID
 * @param $parent (optional)
 *   Parent to max depth of
 * @return
 *   Max depth (distance)
 */
function taxonomy_nested_intervals_get_max_depth($vid, $parent = 0) {
  if ($parent) {
    $point = db_query("SELECT vid, lft, rgt FROM {taxonomy_nested_intervals} WHERE tid = :parent", array(':parent' => $parent))->fetchObject();
    return db_query("SELECT MAX(distance) - :distance FROM {taxonomy_nested_intervals} WHERE vid = :vid AND lft > $point->lft AND lft < $point->rgt", array(
      ':vid' => $vid,
      ':distance' => intval($point->distance),
    ))->fetchField();
  }
  else {
    return db_query("SELECT MAX(distance) FROM {taxonomy_nested_intervals} WHERE vid = :vid", array(
      ':vid' => $vid
    ))->fetchField();
  }
}

// ---------- CORE OVERRIDES ----------

/**
 * Reimplementation of taxonomy_get_tree().
 * Limit db fetch to only specified parent.
 * @see taxonomy_get_tree()
 */
function taxonomy_nested_intervals_get_tree($vid, $parent = 0, $max_depth = NULL, $load_entities = FALSE) {
  if (variable_get("taxonomy_nested_intervals_enabled_$vid", FALSE)) {
    module_load_include('core.inc', 'taxonomy_nested_intervals');
    return taxonomy_nested_intervals_core_get_tree($vid, $parent, $max_depth, $load_entities);
  }
  return FALSE;
}

/**
 * Reimplementation of taxonomy_select_nodes() re-allowing depth modifier.
 */
function taxonomy_nested_intervals_select_nodes($tid, $pager = TRUE, $limit = FALSE, $depth = 0, $order = NULL) {
  $vid = db_query("SELECT vid FROM {taxonomy_term_data} WHERE tid = :tid", array(':tid' => $tid))->fetchField();
  if (variable_get("taxonomy_nested_intervals_enabled_$vid", FALSE)) {
    module_load_include('core.inc', 'taxonomy_nested_intervals');
    return taxonomy_nested_intervals_core_select_nodes($tid, $pager, $limit, $depth, $order);
  }
  return FALSE;
}
