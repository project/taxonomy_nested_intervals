<?php

class MatrixTreeNode {
  public $nv;
  public $dv;
  public $snv;
  public $sdv;

  function dumpMe($title) {
    $org_title = isset($this->title) ? $this->title : NULL;
    $this->title = $title;
    self::printMatrices(array($this));
    if (isset($org_title)) {
      $this->title = $org_title;
    }
    else {
      unset($this->title);
    }
  }

  function __construct($nv, $dv, $snv, $sdv) {
    $this->nv = (string)$nv;
    $this->dv = (string)$dv;
    $this->snv = (string)$snv;
    $this->sdv = (string)$sdv;
  }

  static function getDeterminant() {
    return new self(1, 0, 0, 1);
  }

  static function createFromAssoc(array $data) {
    return new self(
      $data['nv'],
      $data['dv'],
      $data['snv'],
      $data['sdv']
    );
  }

  static function getLeafMatrix($sibling = 1) {
    return new self($sibling, 1, 1, 0);
  }

  static function printMatrices($matrices, $result = NULL, $titles = NULL, $title = NULL) {
    print self::formatMatrices($matrices, $result, $titles, $title);
  }

  static function formatMatrices($matrices, $result = NULL, $titles = NULL, $title = NULL) {
    if ($title) print "$title\n";
    $rows = array();
    foreach ($matrices as $matrix) {
      $rows[-1][] = sprintf(" %7s ", empty($titles) ? (empty($matrix->title) ? '' : $matrix->title) : array_shift($titles));
      $rows[0][] = '+---+---+';
      $rows[1][] = sprintf("|%3s|%3s|", BigNum::floor($matrix->nv), BigNum::floor($matrix->snv));
      $rows[2][] = sprintf("|%3s|%3s|", BigNum::floor($matrix->dv), BigNum::floor($matrix->sdv));
      $rows[3][] = '+---+---+';
    }
    if ($result)  {
      $rows[-1][] = '         ';
      $rows[0][] = '         ';
      $rows[1][] = '   ===   ';
      $rows[2][] = '   ===   ';
      $rows[3][] = '         ';
      $rows[-1][] = sprintf(" %7s ", empty($titles) ? (empty($result->title) ? '' : $result->title) : array_shift($titles));
      $rows[0][] = '+---+---+';
      $rows[1][] = sprintf("|%3s|%3s|", $result->nv, $result->snv);
      $rows[2][] = sprintf("|%3s|%3s|", $result->dv, $result->sdv);
      $rows[3][] = '+---+---+';
    }
    $output = '';
    foreach ($rows as $row) {
      $output .= implode('  ', $row) . "\n";
    }
    return $output;
  }

  /**
   * Convert an enumerated path into a set of matrices.
   */
  static function pathToMatrices($path) {
    $matrices = array();

    if ($path != '') {
      $parts = explode('.', $path);
      foreach ($parts as $part) {
        $matrices[] = self::getLeafMatrix($part);
      }
    }
    if (empty($matrices)) {
      $matrices[] = self::getDeterminant();
    }
    return $matrices;
  }

  /**
   * Convert an enumerated path into a matrix.
   */
  static function pathToMatrix($path) {
    return self::product(self::pathToMatrices($path));
  }

  /**
   * Multiply multiple matrices.
   */
  static function product($matrices) {
    $matrix = array_shift($matrices);
    if (!$matrices) {
      return $matrix;
    }
    foreach ($matrices as $m) {
      $matrix = $matrix->multiply($m);
    }
    return $matrix;
  }

  /**
   * Get an absolute value of the matrix.
   */
  function abs() {
    $class = get_class($this);
    return new $class(
      BigNum::cmp($this->nv, '0') === -1 ? BigNum::mul($this->nv, -1) : $this->nv,
      BigNum::cmp($this->dv, '0') === -1 ? BigNum::mul($this->dv, -1) : $this->dv,
      BigNum::cmp($this->snv, '0') === -1 ? BigNum::mul($this->snv, -1) : $this->snv,
      BigNum::cmp($this->sdv, '0') === -1 ? BigNum::mul($this->sdv, -1) : $this->sdv
    );
  }

  /**
   * Multiply 2 matrices.
   */
  function multiply($matrix) {
    $class = get_class($this);
    return new $class(
      BigNum::add(BigNum::mul($this->nv, $matrix->nv), BigNum::mul($this->snv, $matrix->dv)),
      BigNum::add(BigNum::mul($this->dv, $matrix->nv), BigNum::mul($this->sdv, $matrix->dv)),
      BigNum::add(BigNum::mul($this->nv, $matrix->snv), BigNum::mul($this->snv, $matrix->sdv)),
      BigNum::add(BigNum::mul($this->dv, $matrix->snv), BigNum::mul($this->sdv, $matrix->sdv))
    );
  }

  /**
   * Return an inverse of the matrix
   */
  function getInverse() {
    $class = get_class($this);
    return new $class(
      $this->sdv,
      -$this->dv,
      -$this->snv,
      $this->nv
    );
  }

  function getLeaf() {
    return $this->getLeafMatrix($this->getEnumeratedSibling());
  }

  function getMatrices() {
    $parents = array();
    $parent = $this;
    do {
      $parents[] = $parent->getLeaf();
      $parent = $parent->getParent();
    } while($parent);
    return array_reverse($parents);
  }

  function getPath() {
    $parent = $this;
    do {
      $np[] = $parent->getEnumeratedSibling();
      $parent = $parent->getParent();
    } while($parent);
    return implode('.', array_reverse($np));
  }

  function getDepth() {
    $depth = -1;
    $parent = $this;
    do {
      $depth++;
      $parent = $parent->getParent();
    } while($parent);
    return $depth;
  }

  function getPreviousSibling() {
    $class = get_class($this);
    return new $class($this->nv - $this->snv, $this->dv - $this->sdv, $this->snv, $this->sdv);
  }

  function getNextSibling() {
    $class = get_class($this);
    return new $class($this->nv + $this->snv, $this->dv + $this->sdv, $this->snv, $this->sdv);
  }

  function getSafeParent() {
    $parent = $this->getParent();
    return $parent ? $parent : $this->getDeterminant();
  }

  function getParent() {
    if (BigNum::cmp($this->dv, 0) && BigNum::cmp($this->sdv, 0)) { 
      return $this->multiply($this->getLeaf()->getInverse())->abs();
    }
    return;
  }

  function getEnumeratedSibling() {
    $result = BigNum::cmp($this->snv, 1) === 0 && BigNum::cmp($this->sdv, 0) !== 0 ? $this->dv : BigNum::div($this->nv, $this->snv);
    return intval($result);
  }

  function getLeft($factor = 1) {
    $lft = BigNum::round(BigNum::div(BigNum::mul($this->nv, $factor), $this->dv, 31), 30);
    $rgt = BigNum::round(BigNum::div(BigNum::mul($this->nv + $this->snv, $factor), $this->dv + $this->sdv, 31), 30);
    return BigNum::cmp($lft, $rgt, 30) === -1 ? ($this->getEnumeratedSibling() == 1 ? BigNum::add($lft, '0.000000000000000000000000000001', 30) : $lft) : $rgt;
  }

  function getRight($factor = 1) {
    $lft = BigNum::round(BigNum::div(BigNum::mul($this->nv, $factor), $this->dv, 31), 30);
    $rgt = BigNum::round(BigNum::div(BigNum::mul($this->nv + $this->snv, $factor), $this->dv + $this->sdv, 31), 30);
    return BigNum::cmp($lft, $rgt, 30) === 1 ? $lft : $rgt;
  }

  function generateLeaf($sibling = 1) {
    $class = get_class($this);
    return $this->multiply($class::getLeafMatrix($sibling));
    $result = $class::product(array($this, $class::getLeafMatrix($sibling)));
    $class::printMatrices(array($this, $class::getLeafMatrix($sibling), $result));
    return $result;
  }

  function generateSibling($sibling) {
    $parent = $this->getParent();
    return $parent ? $this->getParent()->generateLeaf($sibling) : self::pathToMatrix("$sibling");
  }

  function relocate($dst, $delta_sibling) {
    return self::product(array(
      $dst->getSafeParent(), // New parent (attach new parent)
      new self(1, 0, $delta_sibling, 1), // Sibling bump
      $this->getSafeParent()->getInverse() // Inverse of old parent (detach old parent)
    ));
  }
}

class MatrixTreeNodeModified extends MatrixTreeNode {

  static function getDeterminant() {
    return new self(0, 1, 1, 0);
  }

  static function createFromAssoc(array $data) {
    return new self(
      $data['nv'],
      $data['dv'],
      $data['snv'],
      $data['sdv']
    );
  }

  static function getLeafMatrix($sibling = 1) {
    return new self(1, $sibling, 1, BigNum::add($sibling, 1));
  }

  static function pathToMatrices($path) {
    $matrices = array();
    $matrices[] = self::getDeterminant();

    if ($path != '') {
      $parts = explode('.', $path);
      foreach ($parts as $part) {
        $matrices[] = self::getLeafMatrix($part);
      }
    }
    return $matrices;
  }

  static function pathToMatrix($path) {
    return self::product(self::pathToMatrices($path));
  }

  function getInverse() {
    $class = get_class($this);
    return new $class(
      -$this->sdv,
      $this->dv,
      $this->snv,
      -$this->nv
    );
  }

  function getMatrices() {
    $parents = array();
    $parent = $this;
    do {
      $parents[] = $parent->getLeaf();
      $parent = $parent->getParent();
    } while($parent);
    $parents[] = self::getDeterminant();
    return array_reverse($parents);
  }

  function getPath() {
    return parent::getPath();

    $path = euclid($this->nv, $this->dv);
    if (count($path) % 2 == 0) {
      array_pop($path);
      array_push($path, 1, 1);
    }
    $np = array();
    for ($idx = 0; $idx < count($path); $idx += 2) {
      $np[] = $path[$idx];
    }
    return implode('.', $np);
  }

  function getDepth() {
    return parent::getDepth();

    return floor(count(euclid($this->nv, $this->dv)) / 2);
  }

  function getPreviousSibling() {
    $class = get_class($this);
    return new $class($this->nv * 2 - $this->snv, $this->dv * 2 - $this->snv, $this->nv, $this->dv);
  }

  function getNextSibling() {
    $class = get_class($this);
    return new $class($this->snv, $this->sdv, $this->snv * 2 - $this->nv, $this->sdv * 2 - $this->dv);
  }

  function getParent() {
    if ($result = parent::getParent()) {
      if (intval($result->nv) == 0) {
        return;
      }
    }
    return $result;
  }

  function getEnumeratedSibling() {
    return intval(BigNum::div($this->nv, BigNum::sub($this->snv, $this->nv)));
  }

  function getLeft($factor = 1) {
    return BigNum::round(BigNum::div(BigNum::mul($this->nv, $factor), $this->dv, 31), 30);
    return BigNum::div(BigNum::mul($this->nv, $factor), $this->dv, 30);
  }

  function getRight($factor = 1) {
    return BigNum::round(BigNum::div(BigNum::mul($this->snv, $factor), $this->sdv, 31), 30);
  }

  function generateSibling($sibling) {
    $parent = $this->getParent();
    return $parent ? $this->getParent()->generateLeaf($sibling) : self::pathToMatrix("$sibling");
  }

  function relocate($dst, $delta_sibling) {
    return self::product(array(
      $dst->getSafeParent(), // New parent (attach new parent)
      new self(1, $delta_sibling, 0, 1), // Sibling bump
      $this->getSafeParent()->getInverse() // Inverse of old parent (detach old parent)
    ));
  }
}

