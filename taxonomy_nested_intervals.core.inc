<?php
/**
 * @file
 *
 * This file contains the core override functions
 */

/**
 * Reimplementation of taxonomy_get_tree().
 * Limit db fetch to only specified parent AND use presorting.
 * @see taxonomy_get_tree()
 */
function taxonomy_nested_intervals_core_get_tree($vid, $parent = 0, $max_depth = NULL, $load_entities = FALSE) {
  $distance = 0;
  if ($parent) {
    $point = db_query("SELECT lft, rgt, distance FROM {taxonomy_nested_intervals} WHERE tid = :parent", array(':parent' => $parent))->fetchObject();
    $distance = $point->distance + 1;
  }
   
  $query = db_select('taxonomy_nested_intervals', 'e');
  $query->join('taxonomy_term_data', 'd', 'd.tid = e.tid');

  $query
    ->addTag('translatable')
    ->addTag('term_access')
    ->fields('e', array('parent'))
    ->fields('d')
    ->condition('e.vid', $vid)
    ->orderBy('e.lft');
  $query->addExpression('e.distance - :distance', 'distance', array(':distance' => $distance));

  if ($parent) {
    $query->where("e.lft > $point->lft AND e.lft < $point->rgt");
  }

  if (isset($max_depth)) {
    $query->condition('e.distance', $max_depth + $distance, '<');
  }

  $result = $query->execute()->fetchAll();

  $tree = array();
  foreach ($result as $term) {
    $term->depth = $term->distance;
    if (!isset($tree[$term->tid])) {
      $tree[$term->tid] = array($term->parent);
    }
    else {
      $tree[$term->tid][] = $term->parent;
      $tree[$term->tid] = array_unique($tree[$term->tid]);
    }
    $term->parents = &$tree[$term->tid];
    unset($term->distance);
    unset($term->parent);
  }

  // Load full entities, if necessary. The entity controller statically
  // caches the results.
  if ($load_entities) {
    $term_entities = taxonomy_term_load_multiple(array_keys($tree));
  }

  // Free memory for $tree
  unset($tree);
  $tree = &$result;

  // Stash entities into tree if necessary
  if ($load_entities) {
    foreach ($tree as $idx => &$term) {
      // Store values depth and parents, because the entity which overwrites
      // does not carry these.
      $t_depth = $term->depth; 
      $t_parents = $term->parents;
      $term = $term_entities[$term->tid];
      // Re-add depth and parents to the term (entity).
      $term->depth = $t_depth;
      $term->parents = $t_parents;
    }
    unset($term_entities);
  }

  return $tree;
}

/**
 * Reimplementation of taxonomy_select_nodes() allowing depth query.
 */
function taxonomy_nested_intervals_core_select_nodes($tid, $pager = TRUE, $limit = FALSE, $depth = 0, $order = NULL) {
  if (!variable_get('taxonomy_maintain_index_table', TRUE)) {
    return array();
  }

  // If no depth, just use core (faster query due to no joins)
  if ($depth == 0) {
    $args = $order ? array($tid, $pager, $limit, $order) : array($tid, $pager, $limit);
    return call_user_func_array('taxonomy_select_nodes', $args);
  }

  // Set default order if not specified
  if (!isset($order)) {
    $order = array('n.sticky' => 'DESC', 'n.created' => 'DESC', 'n.nid' => 'DESC');
  }

  // Lookup point for better query on nested_intervals table
  $point = db_query_range("SELECT vid, lft, rgt, distance FROM {taxonomy_nested_intervals} WHERE tid = :tid", 0, 1, array(':tid' => $tid))->fetchObject();

  // Locate tids to search in
  $subquery = db_select('taxonomy_nested_intervals', 'e');
  $subquery->condition('e.vid', $point->vid);
  $subquery->where("e.lft >= $point->lft AND e.rgt < $point->rgt");
  $subquery->condition('e.distance', $depth + $point->distance, '<=');
  $subquery->fields('e', array('tid'));

  // Find nodes
  $query = db_select('taxonomy_index', 't');
  $query->join('node', 'n', 'n.nid = t.nid');
  $query->addTag('node_access');
  $query->condition('t.tid', $subquery, 'IN');
  if ($pager) {
    $count_query = db_select('taxonomy_index', 't');
    $count_query->join('taxonomy_nested_intervals', 'e', 'e.tid = t.tid');
    $count_query->condition('e.vid', $point->vid);
    $count_query->where("e.lft >= $point->lft AND e.rgt < $point->rgt");
    $count_query->condition('e.distance', $depth + $point->distance, '<=');
    $count_query->addExpression('COUNT(1)');

    $query = $query->extend('PagerDefault');
    if ($limit !== FALSE) {
      $query = $query->limit($limit);
    }
    $query->setCountQuery($count_query);
  }
  else {
    if ($limit !== FALSE) {
      $query->range(0, $limit);
    }
  }
  $query->addField('n', 'nid');
  $query->addField('t', 'tid');
  foreach ($order as $field => $direction) {
    $query->orderBy($field, $direction);
    // ORDER BY fields need to be loaded too, assume they are in the form
    // table_alias.name
    list($table_alias, $name) = explode('.', $field);
    $query->addField($table_alias, $name);
  }
  return $query->execute()->fetchCol();
}
